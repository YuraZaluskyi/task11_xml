<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h1>Tours review</h1>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th rowspan="3">id</th>
                        <th rowspan="3">Type</th>
                        <th rowspan="3">Country</th>
                        <th rowspan="3">Number days/nights</th>
                        <th rowspan="3">Transport</th>
                        <th colspan="6">Hotel characteristics</th>
                    </tr>
                    <tr bgcolor="#2E9AFE">
                        <td rowspan="2"><b>numberStars</b></td>
                        <td rowspan="2"><b>meal type</b></td>
                        <td colspan="4"><b>hotel room</b></td>
                    </tr>

                    <tr bgcolor="#2E9AFE">
                        <td><b>number rooms</b></td>
                        <td><b>TV</b></td>
                        <td><b>AirConditioning</b></td>
                        <td><b>Cost</b></td>
                    </tr>
                    <xsl:for-each select="touristVouchers/touristVoucher">
                    <tr>
                        <td><xsl:value-of select="id"/></td>
                        <td><xsl:value-of select="type"/></td>
                        <td><xsl:value-of select="country"/></td>
                        <td><xsl:value-of select="numberDaysNights"/></td>
                        <td><xsl:value-of select="transport"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/numberStars"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/meal/type"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/hotelRoom/numberRooms"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/hotelRoom/TV"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/hotelRoom/AirConditioning"/></td>
                        <td><xsl:value-of select="hotelCharacteristic/hotelRoom/cost"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

