package com.zaluskyi.comparator;

import com.zaluskyi.model.TouristVoucher;

import java.util.Comparator;

public class TouristVoucherComparartor implements Comparator<TouristVoucher> {
    public int compare(TouristVoucher o1, TouristVoucher o2) {
        return o1.getId() - o2.getId();
    }
}
