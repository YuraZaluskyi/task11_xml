package com.zaluskyi.parser.dom;

import com.zaluskyi.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<TouristVoucher> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<TouristVoucher> touristVouchers = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("touristVoucher");
        for (int i = 0; i < nodeList.getLength(); i++) {
            TouristVoucher touristVoucher = new TouristVoucher();
            HotelCharacteristic hotelCharacteristic;
            Meal meal;
            HotelRoom hotelRoom;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                touristVoucher.setId(Integer.parseInt(element.getAttribute("id")));
                touristVoucher.setType(element.getElementsByTagName("type").item(0).getTextContent());
                touristVoucher.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                touristVoucher.setNumberDaysNights(Integer.parseInt(element.getAttribute("numberDaysNights")));
                touristVoucher.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
            }
        }

        return touristVouchers;
    }

    private Meal getMeal(NodeList nodes) {
        Meal meal = new Meal();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            meal.setTypeMeal((List<TypeMeal>) element.getElementsByTagName("type").item(0).getAttributes());
        }
        return meal;
    }

    private HotelRoom getHotelRoom(NodeList nodes) {
        HotelRoom hotelRoom = new HotelRoom();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            hotelRoom.setTypeHotelRoom((List<TypeHotelRoom>) element.getElementsByTagName("numberRooms").item(0).getAttributes());
            hotelRoom.setTV(Boolean.parseBoolean(element.getAttribute("TV")));
            hotelRoom.setAirConditioning(Boolean.parseBoolean(element.getAttribute("AirConditioning")));

        }
        return hotelRoom;
    }

}
