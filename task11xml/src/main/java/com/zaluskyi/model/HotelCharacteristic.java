package com.zaluskyi.model;

public class HotelCharacteristic {
    private int numberStars;
    private Meal meal;
    private HotelRoom hotelRoom;


    public HotelCharacteristic() {
    }

    public HotelCharacteristic(int numberStars, Meal meal, HotelRoom hotelRoom) {
        this.numberStars = numberStars;
        this.meal = meal;
        this.hotelRoom = hotelRoom;
    }

    public int getNumberStars() {
        return numberStars;
    }

    public void setNumberStars(int numberStars) {
        this.numberStars = numberStars;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public HotelRoom getHotelRoom() {
        return hotelRoom;
    }

    public void setHotelRoom(HotelRoom hotelRoom) {
        this.hotelRoom = hotelRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HotelCharacteristic that = (HotelCharacteristic) o;

        if (numberStars != that.numberStars) return false;
        if (meal != null ? !meal.equals(that.meal) : that.meal != null) return false;
        return hotelRoom != null ? hotelRoom.equals(that.hotelRoom) : that.hotelRoom == null;
    }

    @Override
    public int hashCode() {
        int result = numberStars;
        result = 31 * result + (meal != null ? meal.hashCode() : 0);
        result = 31 * result + (hotelRoom != null ? hotelRoom.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HotelCharacteristic{" +
                "numberStars=" + numberStars +
                ", meal=" + meal +
                ", hotelRoom=" + hotelRoom +
                '}';
    }
}
