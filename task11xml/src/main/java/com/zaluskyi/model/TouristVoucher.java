package com.zaluskyi.model;

public class TouristVoucher {
    private int id;
    private String type;
    private String country;
    private int numberDaysNights;
    private String transport;
    private HotelCharacteristic hotelCharacteristic;
    private int cost;

    public TouristVoucher() {
    }

    public TouristVoucher(int id, String type, String country, int numberDaysNights
            , String transport, HotelCharacteristic hotelCharacteristic, int cost) {
        this.id = id;
        this.type = type;
        this.country = country;
        this.numberDaysNights = numberDaysNights;
        this.transport = transport;
        this.hotelCharacteristic = hotelCharacteristic;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberDaysNights() {
        return numberDaysNights;
    }

    public void setNumberDaysNights(int numberDaysNights) {
        this.numberDaysNights = numberDaysNights;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public HotelCharacteristic getHotelCharacteristic() {
        return hotelCharacteristic;
    }

    public void setHotelCharacteristic(HotelCharacteristic hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TouristVoucher that = (TouristVoucher) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", numberDaysNights=" + numberDaysNights +
                ", transport='" + transport + '\'' +
                ", hotelCharacteristic=" + hotelCharacteristic +
                ", cost=" + cost +
                '}';
    }
}
