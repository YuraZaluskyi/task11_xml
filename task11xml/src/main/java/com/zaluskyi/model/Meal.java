package com.zaluskyi.model;

import java.util.List;

public class Meal {
    private List<TypeMeal> typeMeal;

    public Meal() {
    }

    public Meal(List<TypeMeal> typeMeal) {
        this.typeMeal = typeMeal;
    }

    public List<TypeMeal> getTypeMeal() {
        return typeMeal;
    }

    public void setTypeMeal(List<TypeMeal> typeMeal) {
        this.typeMeal = typeMeal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meal meal = (Meal) o;

        return typeMeal != null ? typeMeal.equals(meal.typeMeal) : meal.typeMeal == null;
    }

    @Override
    public int hashCode() {
        return typeMeal != null ? typeMeal.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "typeMeal=" + typeMeal +
                '}';
    }
}
