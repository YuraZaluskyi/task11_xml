package com.zaluskyi.model;

import java.util.List;

public class HotelRoom {
    private List<TypeHotelRoom> typeHotelRoom;
    private boolean isTV;
    private boolean isAirConditioning;
    private int cost;

    public HotelRoom() {
    }

    public HotelRoom(List<TypeHotelRoom> typeHotelRoom, boolean isTV, boolean isAirConditioning, int cost) {
        this.typeHotelRoom = typeHotelRoom;
        this.isTV = isTV;
        this.isAirConditioning = isAirConditioning;
        this.cost = cost;
    }

    public List<TypeHotelRoom> getTypeHotelRoom() {
        return typeHotelRoom;
    }

    public void setTypeHotelRoom(List<TypeHotelRoom> typeHotelRoom) {
        this.typeHotelRoom = typeHotelRoom;
    }

    public boolean isTV() {
        return isTV;
    }

    public void setTV(boolean TV) {
        isTV = TV;
    }

    public boolean isAirConditioning() {
        return isAirConditioning;
    }

    public void setAirConditioning(boolean airConditioning) {
        isAirConditioning = airConditioning;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HotelRoom hotelRoom = (HotelRoom) o;

        if (isTV != hotelRoom.isTV) return false;
        if (isAirConditioning != hotelRoom.isAirConditioning) return false;
        if (cost != hotelRoom.cost) return false;
        return typeHotelRoom != null ? typeHotelRoom.equals(hotelRoom.typeHotelRoom) : hotelRoom.typeHotelRoom == null;
    }

    @Override
    public int hashCode() {
        int result = typeHotelRoom != null ? typeHotelRoom.hashCode() : 0;
        result = 31 * result + (isTV ? 1 : 0);
        result = 31 * result + (isAirConditioning ? 1 : 0);
        result = 31 * result + cost;
        return result;
    }

    @Override
    public String toString() {
        return "HotelRoom{" +
                "typeHotelRoom=" + typeHotelRoom +
                ", isTV=" + isTV +
                ", isAirConditioning=" + isAirConditioning +
                ", cost=" + cost +
                '}';
    }
}
