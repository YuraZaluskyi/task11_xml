package com.zaluskyi.model;

public enum TypeHotelRoom {
    SNGL, DBL, SUPERIOR, TRPL, QDPL, FAMILY_STUDIO
}
